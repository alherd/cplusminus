#pragma once
#include <string>
#include <vector>
#include <map>
#include <array>
#include "DBFormat.h"

namespace config
{
	using namespace std;

	map<string, DBF::block_body> registered_blocks;

	void init_registered_blocks()
	{
		string name = "basic_rules";
		DBF::block_body body;
		vector<string> values;

		
		values.push_back(";");
		values.push_back("\n");
		body.insert(make_pair("instruction_delemiters", values));
		values.clear();

		values.push_back("main");
		body.insert(make_pair("main_fanction", values));
		values.clear();

		registered_blocks.insert(make_pair(move(name), body));
	}

	void set_config()
	{

	}


} 



