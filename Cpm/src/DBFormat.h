#pragma once
#include <string>
#include <vector>
#include <map>
#include <fstream>

// DBFile - Data in blocks file
/*
*[block_01]
*
*variable_name_01={"value_01","value_02","value_03"};
*variable_name_02={"value_01","value_02","value_03"};
*
*[block_02]
*
*variable_name_01={"value_01","value_02","value_03"};
*variable_name_02={"value_01","value_02","value_03"};
*
*/



namespace DBF // data in blocks format
{
	using namespace std;
	typedef map <string, vector<string>> block_body;




	class file
	{
		string file_name;
		ifstream stream;
		map<string, block_body> data;
		bool was_found = false;
		bool was_init = false;
	public:
		bool is_valid();
		file(string file_path);
		vector<string> get_value(string&& block_name, string&& var_name);
		void init();
		void print_data();
	private:


		string find_block();
		void fill_block_body(string&& block_name);
	};

	void file::print_data()
	{
		bool is_first = true;

		for (auto it : data)
		{	
			if (is_first)
			{
				cout << "block name = " << it.first << endl;
			}
			for (auto it1 : it.second)
			{
				cout << "var_name = " << it1.first << endl;
				for (size_t i = 0; i < it1.second.size(); ++i)
				{
					cout << "value = " << it1.second[i] << endl;
				}
			}
		}
	}


	string file::find_block()
	{
		bool was_found = false;
		char next_char = ' ';
		string block_name;
		enum read_state {find_start,find_end} read_state_o;
		read_state_o = read_state::find_end;
		while (!was_found && stream.get(next_char))
		{
			switch (next_char)
			{
			case '[':
			{
				if (read_state_o == read_state::find_start)
				{
					read_state_o = read_state::find_end;
				}
			}
			break;
			case ']':
			{
				if (read_state_o == read_state::find_end && (block_name.size() > 0))
				{					
					return block_name;
				}
			}
			default:
			{
				//ToDo check for valid char
				block_name.push_back(next_char);
			}
				break;
			}
		}
		return string("");
	}

	void file::fill_block_body(string&& block_name)
	{

		char next_char;
		enum read_state { find_name, find_start_value , find_end_value } read_state_o;
		read_state_o = read_state::find_name;
		string var_name = "";
		string var_value = "";
		vector<string> var_values;
		block_body body;


		while (stream.get(next_char) && next_char != '[')
		{
			switch (next_char)
			{
			case '=':
			{
				if ((read_state_o == read_state::find_name) && (var_name.size() > 0))
				{
					read_state_o = read_state::find_start_value;
				}
			}
			break;
			case '{':
			{

			}
			break;
			case '"':
			{
				if (read_state_o == read_state::find_start_value)
				{
					read_state_o = read_state::find_end_value;
				}
				else
				{
					var_values.push_back(var_value);
					var_value.clear();
					read_state_o = read_state::find_start_value;
				}
			}
			break;
			case ',':
			{

			}
			break;
			case '}':
			{
				if (read_state_o != read_state::find_end_value)
				{
					read_state_o = read_state::find_name;
				}
			}
			break;
			case ';':
			{
				if (read_state_o == read_state::find_end_value)
				{
					var_value.push_back(next_char);
				}
				else
				{
					read_state_o = read_state::find_name;				
					if ((!var_name.empty()) && (var_values.size() > 0))
					{

						body.insert(make_pair(var_name, var_values));
						data.insert(make_pair(block_name, body));

						var_name.clear();
						var_values.clear();
						body.clear();
					}
				}

				


			}
			break;
			case '\n':
			{

			}
			break;
			case ' ':
			{

			}
			break;
			default:
			{
				if (read_state_o == read_state::find_name)
				{
					var_name.push_back(next_char);
				}
				else
				{
					var_value.push_back(next_char);
				}
			}
				break;
			}
		}
	}

	void file::init()
	{
		if (!was_found) return;
		char c;
		string block_name = find_block();
		while (block_name != "")
		{
			fill_block_body(move(block_name));
			block_name = find_block();
		}
		was_init = true;
	}

	bool file::is_valid()
	{
		return was_found && was_init;
	}

	file::file(string file_path)
	{
		stream.open(file_path);
		stream.is_open() ? was_found = true : was_found = false;
	}

	vector<string> file::get_value(string&& block_name, string&& var_name)
	{
		vector<string> value;
		auto block_it = data.find(block_name);
		if (block_it != data.end())
		{
			auto value_it = block_it->second.find(var_name);
			if (value_it != block_it->second.end())
			{
				value = value_it->second;
			}
		}

		return value;
	}
} // namespace DBF



